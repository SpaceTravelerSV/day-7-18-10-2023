
// Palindrome.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream> 

using namespace std;

bool isPalendrome(long long);
int getLength(long long number);	//getLenth - ����� �����


int main()
{
	long long number;

	cout << "Enter the number - natural: ";
	cin >> number;
	if (isPalendrome(number))
	{
		cout << number << " is palindrom";
	}
	else
	{
		cout << "Number is not a palindrom";
	}

}

bool isPalendrome(long long number)
{
	long long power = pow(10, getLength(number)-1);

	while (number != 0)
	{
		int first = number / power;
		int last = number % 10;

		if (first != last)
		{
			return false;
		}

		number %= power;
		number /= 10;
		power /= 100;
	}

	return true;
}

int getLength(long long number)
{
	int count = 0;

	while (number != 0) // ~ while (number) 0 - false 1 - true
	{
		number /= 10;
		count++;
	}

	return count;
}
