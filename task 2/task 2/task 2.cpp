#include <iostream>
using namespace std;

int deleteTheLastNum(unsigned n);

int main()
{
	unsigned n;

	cout << "Enter the number(natural): ";
	cin >> n;

	cout << deleteTheLastNum(n);
}

int deleteTheLastNum(unsigned n)
{
	n /= 10;

	return n;
}